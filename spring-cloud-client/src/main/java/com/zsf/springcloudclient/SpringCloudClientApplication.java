package com.zsf.springcloudclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.refresh.ContextRefresher;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Set;

@SpringBootApplication
public class SpringCloudClientApplication {

    private ContextRefresher contextRefresher;

    @Autowired
    public void RefreshEndpoint(ContextRefresher contextRefresher) {
        this.contextRefresher = contextRefresher;
    }
	public static void main(String[] args) {
		SpringApplication.run(SpringCloudClientApplication.class, args);
	}

    @Scheduled(fixedRate = 3000)
	public void sch(){
        Set<String> refresh = contextRefresher.refresh();
        System.err.println(refresh.toString());
    }
}
